//
//  YoutubeModels.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation

struct YVideoListResponse: JSONModel {
    let kind, etag: String?
    let pageInfo: PageInfo?
    let items: [VideoItem]
}

struct VideoItem: JSONModel {
    let kind, etag, id: String
    let snippet: Snippet
    
    var hqdefault : String{
        return "https://img.youtube.com/vi/\(id)/hqdefault.jpg"
    }
}

struct Snippet: JSONModel {
    let publishedAt, channelID, title: String
    let description: String?
    let thumbnails: Thumbnails
    let channelTitle: String?
    let tags: [String]?
    let categoryID, liveBroadcastContent: String?
    let localized: Localized?
    
    enum CodingKeys: String, CodingKey {
        case publishedAt
        case channelID = "channelId"
        case title, description, thumbnails, channelTitle, tags
        case categoryID = "categoryId"
        case liveBroadcastContent, localized
    }
}

struct Localized: JSONModel {
    let title, description: String
}

struct Thumbnails: JSONModel {
    let _default, medium, high, standard: Thumbnail?
    let maxres: Thumbnail?
    
    enum CodingKeys: String, CodingKey {
        case _default = "default"
        case medium, high, standard, maxres
    }
    
    var maxThumnail:Thumbnail?{
        return [maxres,standard,high,medium,_default].compactMap{$0}.first
    }
}

struct Thumbnail: JSONModel {
    let url: String
    let width, height: Int
}

struct PageInfo: JSONModel {
    let totalResults, resultsPerPage: Int
}
