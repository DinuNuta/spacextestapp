//
//  Launch.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/22/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation

struct Launch: JSONModel {
    let flightNumber: Int
    let missionName: String
    let missionIDs: [String]
    let upcoming: Bool
    
    let details: String?
    
    let lunchYear: String
    let launchDateUnix: Double
    let launchDateUtc: String?
    let launchDateLocal: String?
    let isTentative: Bool?
    let tentativeMaxPrecision: String
    let ships: [String]
    
    let launchSuccess: Bool?
    let staticFireDateUtc: String?
    let staticFireDateUnix: Double?
    let rocket: Rocket
    let links: Links
    let reuse: Reuse?
    let launchSite: LaunchSite
    let telemetry: Telemetry
    let tbd: Bool
    
    enum CodingKeys: String, CodingKey {
        case flightNumber = "flight_number"
        case missionName = "mission_name"
        case missionIDs = "mission_id"
        case upcoming = "upcoming"
        case details = "details"
        case lunchYear = "launch_year"
        case launchDateUnix = "launch_date_unix"
        case launchDateUtc = "launch_date_utc"
        case launchDateLocal = "launch_date_local"
        case isTentative = "is_tentative"
        case tentativeMaxPrecision = "tentative_max_precision"
        case ships = "ships"
        case launchSuccess = "launch_success"
        case reuse
        case staticFireDateUtc = "static_fire_date_utc"
        case staticFireDateUnix = "static_fire_date_unix"
        case rocket
        case links
        case telemetry
        case tbd
        case launchSite = "launch_site"
    }
    
    var videoId:String?{
        guard let _videoLink = self.links.videoLink else {return nil}
        return URL(string: _videoLink)?
            .queryItems
            .first(where: { $0.name == "v"})?
            .value
    }
    
}

struct Telemetry: JSONModel {
    let flightClub: String?
    
    enum CodingKeys: String, CodingKey {
        case flightClub = "flight_club"
    }
}

struct Links: JSONModel {
    let missionPatch, missionPatchSmall: String?
    let redditCampaign, redditLaunch: String?
    let redditRecovery: String?
    let redditMedia: String?
    let presskit: String?
    let articleLink, wikipedia, videoLink: String?
    let flickrImages: [String]
    
    enum CodingKeys: String, CodingKey {
        case missionPatch = "mission_patch"
        case missionPatchSmall = "mission_patch_small"
        case redditCampaign = "reddit_campaign"
        case redditLaunch = "reddit_launch"
        case redditRecovery = "reddit_recovery"
        case redditMedia = "reddit_media"
        case presskit
        case articleLink = "article_link"
        case wikipedia
        case videoLink = "video_link"
        case flickrImages = "flickr_images"
    }
}

struct LaunchSite: JSONModel {
    let siteID, siteName, siteNameLong: String
    
    enum CodingKeys: String, CodingKey {
        case siteID = "site_id"
        case siteName = "site_name"
        case siteNameLong = "site_name_long"
    }
}

struct Reuse: JSONModel {
    let core, sideCore1, sideCore2, fairings: Bool
    let capsule: Bool
    
    enum CodingKeys: String, CodingKey {
        case core
        case sideCore1 = "side_core1"
        case sideCore2 = "side_core2"
        case fairings, capsule
    }
}

struct Rocket: JSONModel {
    let id: String
    let name: String
    let rocketType: String
    let firstStage: FirstStage
    let secondStage: SecondStage
    let fairings: Fairings?
    enum CodingKeys: String, CodingKey {
        case id = "rocket_id"
        case name = "rocket_name"
        case rocketType = "rocket_type"
        case firstStage = "first_stage"
        case secondStage = "second_stage"
        case fairings
    }
}

struct Fairings: JSONModel {
    let reused, recoveryAttempt, recovered: Bool?
    let ship: String?
    
    enum CodingKeys: String, CodingKey {
        case reused
        case recoveryAttempt = "recovery_attempt"
        case recovered, ship
    }
    
}


struct FirstStage: JSONModel {
    let cores: [Core]
    
    enum CodingKeys: String, CodingKey {
        case cores
    }
}

struct Core: JSONModel {
    let coreSerial: String?
    let flight: Int?
    let block: Int?
    let gridfins: Bool?
    let legs: Bool?
    let reused: Bool?
    let landSuccess: Bool?
    let landingIntent: Bool?
    let landingType: String?
    let landingVehicle: String?
    
    enum CodingKeys: String, CodingKey {
        case coreSerial = "core_serial"
        case flight, block, gridfins, legs, reused
        case landSuccess = "land_success"
        case landingIntent = "landing_intent"
        case landingType = "landing_type"
        case landingVehicle = "landing_vehicle"
    }
}


struct SecondStage: JSONModel {
    let block:Int?
    let payloads: [Payload]
    enum CodingKeys: String, CodingKey{
        case block
        case payloads
    }
}

struct Payload: JSONModel {
    let payloadID: String
    let noradID: [Int]
    let reused: Bool
    let customers: [String?]
    let nationality, manufacturer, payloadType: String?
    let payloadMassKg, payloadMassLbs: Float?
    let orbit: String
    
    enum CodingKeys: String, CodingKey {
        case payloadID = "payload_id"
        case noradID = "norad_id"
        case reused, customers, nationality, manufacturer
        case payloadType = "payload_type"
        case payloadMassKg = "payload_mass_kg"
        case payloadMassLbs = "payload_mass_lbs"
        case orbit
    }
}


