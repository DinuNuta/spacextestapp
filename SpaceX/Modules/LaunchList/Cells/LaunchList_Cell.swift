//
//  LaunchList_Cell.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/25/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import UIKit
import CDUtilites
import Kingfisher

struct LaunchCellViewModele {
    let title : String
    let formatedDate : String
    let thumbnailURL : URL?
}


extension LaunchList_Cell: NibLoadableView{}

class LaunchList_Cell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 15
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func config(with viewModel: LaunchCellViewModele) {
//        thumbnailImageView.kf.setImage(with: viewModel.thumbnailURL)
        thumbnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: UIImage(named: "img-placeholder"), options:nil , progressBlock: nil, completionHandler: nil)
        DispatchQueue.main.async {
            self.titleLabel.text = viewModel.title
            self.dateLabel.text = viewModel.formatedDate
        }
    }
}
