//
//  LaunchList_VC.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import UIKit
import CDViper
import ESPullToRefresh

protocol LaunchList_ViewOutput {
    func numberOfItems() -> Int
    func item(at indexPath:IndexPath)->LaunchCellViewModele
    func didSelectItem(at indexPath:IndexPath)
    func pullToRefresh()
    func getNextPage()
}

protocol LaunchList_ViewInput:class {
    func reloadTable()
    func didEndPullToRefresh()
    func didEndInfinitScroll(insertedIndx:[IndexPath])
}

class LaunchList_VC: ViperTableViewController {

    var output: LaunchList_ViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        self.tableView.es.startPullToRefresh()
        
    }

    
    // MARK: - Table view data source

    func configTableView(){
        self.tableView.register(LaunchList_Cell.self)
        self.tableView.rowHeight = 240
        
        tableView.es.addPullToRefresh {[weak self] in
            self?.output.pullToRefresh()
        }
        
        tableView.es.addInfiniteScrolling {[weak self] in
            self?.output.getNextPage()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfItems()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LaunchList_Cell
        let viewModel = output.item(at: indexPath)
        cell.config(with: viewModel)
        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.didSelectItem(at: indexPath)
    }
}

extension LaunchList_VC: LaunchList_ViewInput{
    func reloadTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func didEndPullToRefresh() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.es.stopPullToRefresh()
            self.tableView.es.stopLoadingMore()
        }
    }
    func didEndInfinitScroll(insertedIndx:[IndexPath]) {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: insertedIndx, with: .fade)
            self.tableView.endUpdates()
            self.tableView.es.stopLoadingMore()
            if insertedIndx.isEmpty {
                self.tableView.es.noticeNoMoreData()
            }else{
                
            }
        }
    }
    
}
