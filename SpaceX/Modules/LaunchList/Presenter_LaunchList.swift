//
//  Presenter_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDResult

class Presenter_LaunchList {
    // DI --------------------------------------------------------------
    weak var view: LaunchList_ViewInput!
    var interactor: LaunchList_Interactor_Input!
    var router: LaunchList_Router_Input!
    // -----------------------------------------------------------------
    
    var launches = [Launch]()
    var ydataItems = [VideoItem]()
    var currentPage = 0
    let pageLimit = 50
    
    var dateformater : DateFormatter = {
        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.timeZone = TimeZone.autoupdatingCurrent
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSZ"
        return dateFormatterGet
    }()
    
    var printDateformater : DateFormatter = {
        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.timeZone = TimeZone.autoupdatingCurrent
        dateFormatterGet.dateFormat = " MMMM d, YYYY "
        return dateFormatterGet
    }()
}


extension Presenter_LaunchList: LaunchList_ViewOutput {
    func pullToRefresh() {
        currentPage = 0
        interactor.getLaunches()
    }
    
    func getNextPage() {
        let at = min(currentPage*pageLimit, launches.count)
        currentPage += 1
        let to = min(currentPage*pageLimit, launches.count)
        if ![at ..< to].isEmpty{
            let vids = launches[at ..< to].compactMap({$0.videoId})
            interactor.getLaunches(with: vids)
        }else{
            view.didEndInfinitScroll(insertedIndx: [])
        }
        
    }
    
    func item(at indexPath: IndexPath) -> LaunchCellViewModele {
        let launch = launches[indexPath.row]
        let yItem = ydataItems.first(where: {$0.id == launch.videoId})
        let thumnails = yItem?.snippet.thumbnails
        let iconUrl = URL(string: thumnails?.maxThumnail?.url ?? yItem?.hqdefault ?? "")
        
        var formatedString = ""
        let launchDate = Date.init(timeIntervalSince1970: launch.launchDateUnix)
            formatedString = printDateformater.string(from: launchDate)
        
        
        return LaunchCellViewModele.init(title: launch.missionName, formatedDate: formatedString, thumbnailURL: iconUrl)
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        let launch = launches[indexPath.row]
        router.openLaunchDetails(with: launch)
    }
    
    func numberOfItems() -> Int {
        return min(currentPage*pageLimit, launches.count)
    }
}

extension Presenter_LaunchList: LaunchList_Interactor_Output{
    func didGetVideoSnippets(result: Result<[VideoItem]>) {
        do {
            let items = try result.resolve()
            if currentPage == 1 {
                ydataItems = items
                view.didEndPullToRefresh()
            }else{
                let lastCount = ydataItems.count
                ydataItems.append(contentsOf: items)
                
                var indexPaths = [IndexPath]()
                for indx in lastCount ..< ydataItems.count{
                    indexPaths.append(IndexPath(item: indx, section: 0))
                }
                view.didEndInfinitScroll(insertedIndx: indexPaths)
            }
        } catch {
            print(error)
        }
    }
    
    func didGetLaunches(result: Result<[Launch]>) {
        do {
            launches = try result.resolve()
            getNextPage()
        } catch {
            print(error)
        }
    }
}
