//
//  Router_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/25/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDViper

protocol LaunchList_Router_Input {
    func openLaunchDetails(with launch:Launch)
}

struct Router_LaunchList: LaunchList_Router_Input {
    weak var transitionHandler: ViperModuleTransitionHandler!

    func openLaunchDetails(with launch:Launch){
        DispatchQueue.main.async {
            self.transitionHandler.openModule(defaultSegueIdentifierFor: LaunchDetails_VC.self,
                                         configurator: Builder_LaunchDetails(),
                                         customTransitioningDelegate: nil) { (input) in
                                            guard let input = input as? LaunchDetailsModuleInput else {return}
                                            input.config(with: launch)
            }
        }
    }
}
