//
//  Interactor_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDResult

protocol LaunchList_Interactor_Input {
    func getLaunches()
    func getLaunches(with vIds:[String])
}

protocol LaunchList_Interactor_Output:class {
    func didGetLaunches(result: Result<[Launch]>)
    func didGetVideoSnippets(result: Result<[VideoItem]>)
}

struct Interactor_LaunchList : LaunchList_Interactor_Input {
    // DI --------------------------------------------------------------
    weak var output: LaunchList_Interactor_Output?
    let launchFetcher: LaunchFetching
    let videoSnipetFetcher: YVideoSnipetFetching
    // -----------------------------------------------------------------
    
    func getLaunches() {
        launchFetcher.fetchLaunches {[weak output = output](result) in
            output?.didGetLaunches(result: result)
        }
    }
    
    func getLaunches(with vIds:[String]) {
        videoSnipetFetcher.fetchVideos(with: vIds) {[weak output = output] (result) in
            do {
                let response = try result.resolve()
                output?.didGetVideoSnippets(result: .success(response.items))
            }
            catch {
                output?.didGetVideoSnippets(result: .error(error))
            }
        }
    }
    
}
