//
//  Builder_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDViper

class LaunchList_Initializer: NSObject{
    
    @IBOutlet weak var viewInput: LaunchList_VC!
    
    override func awakeFromNib() {
        Builder_LaunchList().configure(with: viewInput)
    }
}


struct Builder_LaunchList: ViperModuleConfigurator {
    
    func configure(with viewInput: UIViewController) {
        guard let viewInput = viewInput as? LaunchList_VC else {return}
        
        let presenter = Presenter_LaunchList()
        let interactor = Interactor_LaunchList.init(output: presenter,
                                                    launchFetcher: LaunchFetcher(),
                                                    videoSnipetFetcher: YVideoSnipetFetcher())
        let router = Router_LaunchList.init(transitionHandler: viewInput)

        presenter.interactor = interactor
        presenter.view = viewInput
        presenter.router = router
        viewInput.output = presenter
    }
}
