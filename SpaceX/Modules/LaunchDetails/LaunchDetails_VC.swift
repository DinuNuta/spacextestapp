//
//  LaunchList_VC.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import UIKit
import CDViper
import youtube_ios_player_helper

struct LaunchDetailsViewModel{
    let title: String
    let description: String?
    let launcheDate: String?
    let rocketName: String?
    let mass: String?
    let videoID:String?
    let wikipediaLink: String?
    
}


protocol LaunchDetails_ViewOutput{
    func viewDidLoad()
}

protocol LaunchDetails_ViewInput:class {
    func config(with viewModel:LaunchDetailsViewModel)
}

class LaunchDetails_VC: ViperViewController {
   
    @IBOutlet weak var launcheDateLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var rocketNameLabel: UILabel!
    @IBOutlet weak var payloadmassLabel: UILabel!
    @IBOutlet weak var playerView: YTPlayerView!
    @IBOutlet weak var wikipediaButton: UIButton!
    
    var output: LaunchDetails_ViewOutput!
    
    override var moduleInput: ViperModuleInput? {
        return output as? ViperModuleInput
    }
    
    var wikipediaLink: URL?{
        didSet{
            self.wikipediaButton.isHidden = wikipediaLink == nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.playerView.stopVideo()
    }
    
    @IBAction func openWikipedia(_ sender: Any) {
        if let _url = wikipediaLink{
            UIApplication.shared.open(_url, options: [:], completionHandler: nil)
        }
    }
    

}

extension LaunchDetails_VC: LaunchDetails_ViewInput{
    func config(with viewModel: LaunchDetailsViewModel) {
        
        wikipediaLink = URL.init(string: viewModel.wikipediaLink ?? "https://en.wikipedia.org/wiki")
        DispatchQueue.main.async {
            _ = viewModel.videoID.flatMap{self.playerView.load(withVideoId: $0)}  
            self.title = viewModel.title
            self.launcheDateLabel.text = viewModel.launcheDate
            self.rocketNameLabel.text = "Rocket name: " + (viewModel.rocketName ?? "-")
            self.descriptionTextView.text = viewModel.description
            self.payloadmassLabel.text = "Payload mass: " + (viewModel.mass ?? "-")
        }
    }
    
    
}
