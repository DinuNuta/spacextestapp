//
//  Builder_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDViper

class LaunchDetails_Initializer: NSObject{
    
    @IBOutlet weak var viewInput: LaunchDetails_VC!
    
    override func awakeFromNib() {
        Builder_LaunchDetails().configure(with: viewInput)
    }
}


struct Builder_LaunchDetails: ViperModuleConfigurator {
    
    func configure(with viewInput: UIViewController) {
        guard let viewInput = viewInput as? LaunchDetails_VC else {return}
        
        let presenter = Presenter_LaunchDetails()
        
        presenter.view = viewInput        
        viewInput.output = presenter
    }
}
