//
//  Presenter_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDViper

protocol LaunchDetailsModuleInput: ViperModuleInput {
    func config(with launch: Launch)
}

class Presenter_LaunchDetails {
    // DI --------------------------------------------------------------
    weak var view: LaunchDetails_ViewInput!    
    // -----------------------------------------------------------------
    
    var launch: Launch?
    
    var printDateformater : DateFormatter = {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = " MMMM d, YYYY "
        return dateFormatterGet
    }()
}

extension Presenter_LaunchDetails: LaunchDetailsModuleInput {
    func config(with launch: Launch) {
        self.launch = launch
        
    }
}


extension Presenter_LaunchDetails: LaunchDetails_ViewOutput {
    
    func viewDidLoad() {
        guard let launch = self.launch else {return}
        let date = Date.init(timeIntervalSince1970: launch.launchDateUnix)
        let formatedDate = printDateformater.string(from: date)
        let mass = launch.rocket.secondStage.payloads.first?.payloadMassKg
        
        let viewModel = LaunchDetailsViewModel.init(title: launch.missionName,
                                                    description: launch.details,
                                                    launcheDate: formatedDate,
                                                    rocketName: launch.rocket.name,
                                                    mass: String("\(mass ?? 0) kg"),
                                                    videoID: launch.videoId,
                                                    wikipediaLink: launch.links.wikipedia)
        self.view.config(with: viewModel)
    }
   
}
