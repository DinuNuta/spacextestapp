//
//  Router_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/25/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDViper

protocol LaunchDetails_Router_Input {
    
}

struct Router_LaunchDetails: LaunchDetails_Router_Input {
     weak var transitionHandler: ViperModuleTransitionHandler!
}
