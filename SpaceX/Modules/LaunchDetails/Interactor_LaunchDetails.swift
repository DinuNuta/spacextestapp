//
//  Interactor_LaunchList.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation

protocol LaunchDetails_Interactor_Input {
    
}

protocol LaunchDetails_Interactor_Output:class {
    
}

struct Interactor_LaunchDetails : LaunchDetails_Interactor_Input {
    weak var output: LaunchDetails_Interactor_Output?
}
