//
//  YAcountKey.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation

typealias YAC = YAcountCredentails

struct YAcountCredentails{
    static let appKey : String = "AIzaSyBR2fJzTjYOabgnyiCYjqDPUK_owFaGf_Y"
    
    enum reqParam:String {
        case key
        case part
        case id
        
        var _defValue:String{
            switch self {
            case .part:
                return "snippet"
            case .key:
                return YAcountCredentails.appKey
            default:
                return ""
            }
        }
    }
}
