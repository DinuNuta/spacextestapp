//
//  JSONModel.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/22/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation

public enum JSONParseError : Error {
    case failedToParse(message:String)
}


public protocol JSONModel : Codable {
    init(data: Data?, keyPath: String?) throws
}

public extension JSONModel {
    public init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw JSONParseError.failedToParse(message:"Data is nil") }
        
        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else { throw JSONParseError.failedToParse(message:"Didn't found object for keypath : \(keyPath)") }
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try JSONDecoder().decode(Self.self, from: nestedData)
            return
        }
        self = try JSONDecoder().decode(Self.self, from: data)
    }
}

public extension Array where Element : JSONModel {
    public init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw JSONParseError.failedToParse(message:"Data is nil" ) }
        
        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else { throw JSONParseError.failedToParse(message:"Didn't found object for keypath : \(keyPath)")}
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try JSONDecoder().decode([Element].self, from: nestedData)
            return
        }
        self = try JSONDecoder().decode([Element].self, from: data)
    }
}
