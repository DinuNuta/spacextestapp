//
//  URLs.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/22/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation

struct URLs {
    static var base = URL(string: "https://api.spacexdata.com/v3/")!
    static var launches = base.appendingPathComponent("launches")
    static var getRocketsInfo = base.appendingPathComponent("rockets")
    
    static var baseYv3 = URL(string: "https://www.googleapis.com/youtube/v3/")!
    static var yVideos = baseYv3.appendingPathComponent("videos")
    
    
    
}

public extension URL {
    public func appendingQuery(withParameters parameters:[URLQueryItem]) -> URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        if components.queryItems != nil {
            components.queryItems?.append(contentsOf: parameters)
        } else {
            components.queryItems = parameters }
        return components.url!
    }
    
    public var queryItems: [URLQueryItem] {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        return components.queryItems ?? []
    }
    
}

