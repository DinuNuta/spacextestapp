//
//  YVideoSnipetFetcher.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDResult
import CDNetwork

protocol YVideoSnipetFetching {
    func fetchVideos(with vIds:[String], completion: @escaping(Result<YVideoListResponse>)->())
}

struct YVideoSnipetFetcher: YVideoSnipetFetching {
    var requestSender : CDRequestSending = CDRequestSender()
    
    func fetchVideos(with vIds: [String], completion: @escaping (Result<YVideoListResponse>) -> ()) {
        
        let url = URLs.yVideos.appendingQuery(withParameters: snippetqItems(with: vIds))
        let request = CDRequest(with: .GET, url: url)
        requestSender.send(request: request) { (result) in
            do {
                let data = try result.resolve()
                completion(.success(try YVideoListResponse(data: data)))
            } catch {
                completion(.error(error))
            }
        }
    }
    
    private func snippetqItems(with ids:[String])->[URLQueryItem]{
        let qItem_key = URLQueryItem(name: YAC.reqParam.key.rawValue, value: YAC.appKey)
        let qItem_part = URLQueryItem(name: YAC.reqParam.part.rawValue, value: YAC.reqParam.part._defValue)
        let strIds = ids.joined(separator: ",")
        let qItem_Ids = URLQueryItem(name: YAC.reqParam.id.rawValue, value: strIds)
        
        return [qItem_key, qItem_part, qItem_Ids]
    }
}
