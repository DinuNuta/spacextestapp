//
//  LaunchFetcher.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 11/23/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDResult
import CDNetwork

protocol LaunchFetching {
    func fetchLaunches(with completion: @escaping(Result<[Launch]>)->())
}

struct LaunchFetcher: LaunchFetching {
    var requestSender : CDRequestSending = CDRequestSender()
    
    func fetchLaunches(with completion: @escaping (Result<[Launch]>) -> ()) {
        let desc = URLQueryItem(name: "order", value: "desc")
        let url = URLs.launches.appendingQuery(withParameters:[desc])
        let request = CDRequest(with: .GET, url: url)
        requestSender.send(request: request) { (result) in
            do {
                let data = try result.resolve()
                let json = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.allowFragments])
                print(json)
                completion(.success(try [Launch](data:data)))
            } catch {
                completion(.error(error))
            }
        }
    }
}
