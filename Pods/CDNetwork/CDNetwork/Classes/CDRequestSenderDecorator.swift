//
//  CDRequestSenderDecorator.swift
//  Pods
//
//  Created by Coscodan Dinu on 8/24/17.
//
//

import Foundation
import CDResult

public protocol CDRequestSenderDecorator : CDRequestSending{
    var target : CDRequestSending {get set}
}

public extension CDRequestSenderDecorator{
    
    public var errorInterpreter: CDErrorInterpreting?
    {
        get { return target.errorInterpreter }
        set { target.errorInterpreter = newValue }
    }
    
    public func send(request: CDRequestType, completion: ((Result<Data?>) -> ())?)
    {
        target.send(request: request, completion: completion)
    }
}
