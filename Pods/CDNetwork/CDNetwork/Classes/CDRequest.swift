//
//  CDRequest.swift
//  Pods
//
//  Created by Coscodan Dinu on 8/24/17.
//
//

import Foundation

public enum HttpMethod : String
{
    case POST
    case PUT
    case GET
    case PATCH
    case DELETE
}

public protocol CDRequestType
{
    var url     : URL {get}
    var method  : HttpMethod {get}
    var request : URLRequest {get}
}

public struct CDRequest : CDRequestType
{
    public var method: HttpMethod
    public var url: URL
    
    public var request: URLRequest{
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
    
    public init(with method: HttpMethod, url: URL)
    {
        self.method = method
        self.url = url
    }
}
