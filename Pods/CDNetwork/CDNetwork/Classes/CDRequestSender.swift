//
//  CDRequestSender.swift
//  Pods
//
//  Created by Coscodan Dinu on 8/24/17.
//
//

import Foundation
import CDResult

public protocol CDRequestSending
{
    var errorInterpreter : CDErrorInterpreting? {get set}
    
    /// Sending custom request and then execute completion block
    ///
    /// - Parameters:
    ///   - request: CustomRequestType
    ///   - completion: completion block will execute after get response
    func send(request : CDRequestType, completion: ( (Result<Data?>) -> () )?)
}

public extension CDRequestSending
{
    public var errorInterpreter : CDErrorInterpreting?{
        return nil
    }
}

public struct CDRequestSender : CDRequestSending
{
    public var errorInterpreter: CDErrorInterpreting?
    
    public init(with errorInterpreter : CDErrorInterpreting? = nil){
        self.errorInterpreter = errorInterpreter
    }
    
    public func send(request: CDRequestType, completion: ((Result<Data?>) -> ())?)
    {
        let urlRequest = request.request
        URLSession(configuration:URLSessionConfiguration.default).dataTask(with: urlRequest)
        { (data, response, error) in
            if let error = error
            {
                completion?(Result.error(error))
                return //
            }
            if let networkError = self.errorInterpreter?.error(with: response, data: data)
            {
                completion?(Result.error(networkError))
            }
            else
            {
                completion?(Result.success(data))
            }
            }.resume()
    }
}
