//
//  DVExtensions.swift
//  Evrica
//
//  Created by Coscodan Dinu on 5/17/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation
import UIKit

//MARK: - extension NSLayoutConstraint Multiplier

public extension NSLayoutConstraint
{
    /// Change multiplier constraint
    ///
    /// - Parameter multiplier: CGFloat  The constant multiplied with the attribute on the right side of the constraint as part of getting the modified attribute.
    /// - Returns: NSLayoutConstraint
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint
    {
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}



//MARK: - UIView Extension Border / Corner

public extension UIView
{
    @IBInspectable var borderColor : UIColor?
        {
        set (newValue) {  layer.borderColor = (newValue?.cgColor ?? tintColor.cgColor) }
        get{ return UIColor(cgColor: layer.borderColor!) }
    }
    
    @IBInspectable var borderWidth : CGFloat
        {
        set (newValue) { layer.borderWidth = newValue }
        get{ return layer.borderWidth }
    }
    
    @IBInspectable var cornerRadius : CGFloat
        {
        set{ layer.cornerRadius = newValue }
        get{ return layer.cornerRadius }
    }
}

public extension UIButton
{
    @IBInspectable var HcontentAlign : Int
        {
        set{ contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.init(rawValue: newValue)! }
        get{ return contentHorizontalAlignment.rawValue }
    }
}


// MARK: - CollectionView / UItableRegister ReusableView for cell

extension UICollectionViewCell: ReusableView { }

extension UITableViewCell: ReusableView { }
extension UITableViewHeaderFooterView: ReusableView { }

public protocol ReusableView: class { }

public extension ReusableView where Self: UIView
{
    static var reuseIdentifier: String
    {
        return String(describing: self)
    }
}

public protocol NibLoadableView: class { }

public extension NibLoadableView where Self: UIView
{
    static var NibName: String
    {
        return String(describing: self)
    }
    static var bundle: Bundle?{
        return Bundle(for: self)
    }
}

public extension NibLoadableView where Self: UIViewController
{
    static var NibName: String
    {
        return String(describing: self)
    }
    static var bundle: Bundle?{
        return Bundle(for: self)
    }
}

public extension UITableView {
    
    func scrollToBottom(){
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}


// MARK: - UICollectionView register cell ReusableView
public extension UICollectionView
{
    func register<T: UICollectionViewCell>(_: T.Type) where T: NibLoadableView
    {
        let Nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(Nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T
    {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else
        {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
}

// MARK: - UICollectionView register HeaderFooterView OfKind
public extension UICollectionView
{
    func register<T: UICollectionReusableView>(_: T.Type, forSupplementaryViewOfKind kind: String) where T: ReusableView, T: NibLoadableView
    {
        let Nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(Nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(OfKind kind: String, forIndexPath indexPath: IndexPath) -> T where T: ReusableView
    {
        guard let view = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else
        {
            fatalError("Could not dequeue view with identifier: \(T.reuseIdentifier)")
        }
        return view
    }
}

// MARK: - UITableView register cell ReusableView
public extension UITableView
{
    
    func register<T: UITableViewCell>(_: T.Type) where T: NibLoadableView
    {
        let Nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(Nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T
    {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T
            else {
                fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    
}

// MARK: - UITableView register HeaderFooterView
public extension UITableView
{
    func register<T: UITableViewHeaderFooterView>(_: T.Type) where T: NibLoadableView
    {
        let Nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(Nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusebleHeaderFooterView<T: UITableViewHeaderFooterView>() -> T
    {
        guard let view = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T
            else {
                fatalError("Could not dequeue Header or Footer view with identifier: \(T.reuseIdentifier)")
        }
        return view
    }

}



//MARK: - CustomView

public protocol CustomView: NibLoadableView
{
    var view: UIView! {get set}
}

public extension CustomView where Self: UIView {
    func xibSetup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        addSubview(view)
    }
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: Self.NibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}

//MARK: - Array
public extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result : [Element]=[]
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

//MARK: - Get current Top ViewController

public extension UIApplication
{
    public class func topViewController(base: UIViewController? = (UIApplication.shared.delegate)?.window??.rootViewController) -> UIViewController?
    {
        if let nav = base as? UINavigationController
        {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController
        {
            if let selected = tab.selectedViewController
            {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController
        {
            return topViewController(base: presented)
        }
        return base
    }
}


// MARK: - Date Extension String offset from date
public extension Date {
    func yearsFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    func monthsFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    func weeksFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    func daysFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    func hoursFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 00
    }
    func minutesFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    func secondsFrom(date: Date) -> Int
    {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    func offsetFrom(date: Date) -> String
    {
        if yearsFrom(date: date)   > 0 { return "\(yearsFrom(date: date)) " + "years".localized }
        if monthsFrom(date: date)  > 0 { return "\(monthsFrom(date: date)) " + "months".localized }
        if weeksFrom(date: date)   > 0 { return "\(weeksFrom(date: date)) " + "weeks".localized }
        if daysFrom(date: date)    > 0 { return "\(daysFrom(date: date)) " + "days".localized }
        if hoursFrom(date: date)   > 0 { return "\(hoursFrom(date: date)) " + "hours".localized }
        if minutesFrom(date: date) > 0 { return "\(minutesFrom(date: date)) " + "mins".localized }
        if secondsFrom(date: date) > 0 { return "\(secondsFrom(date: date)) " + "sconds".localized  }
        return ""
    }
}

// MARK: - Number with separator
public struct Number {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "." // or possibly "," / " "
        formatter.numberStyle = .decimal
        return formatter
    }()
}
extension BinaryInteger {
    var stringWithSepator: String
    {
        return Number.withSeparator.string(from: NSNumber(value: hashValue)) ?? ""
    }
}

public extension Numeric {
    func currency(numberStyle: NumberFormatter.Style = NumberFormatter.Style.currency, locale: Locale = Locale.current, groupingSeparator: String? = nil, decimalSeparator: String? = nil) -> String?  {
        if let num = self as? NSNumber {
            let formater = NumberFormatter()
            formater.locale = locale
            formater.numberStyle = numberStyle
            var formatedSting = formater.string(from: num)
            if let separator = groupingSeparator, let localeValue = locale.groupingSeparator {
                formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
            }
            if let separator = decimalSeparator, let localeValue = locale.decimalSeparator  {
                formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
            }
            return formatedSting
        }
        return nil
    }
    
    func formatUsingAbbrevation () -> String? {
        if let num = self as? NSNumber {
        let numFormatter = NumberFormatter()
        
        typealias Abbrevation = (threshold:Double, divisor:Double, suffix:String)
        let abbreviations:[Abbrevation] = [(0, 1, ""),
                                           (1000.0, 1000.0, "K"),
                                           (100_000.0, 1_000_000.0, "M"),
                                           (100_000_000.0, 1_000_000_000.0, "B")]
        // you can add more !
        let startValue = abs(Double(num.doubleValue))
        let abbreviation:Abbrevation = {
            var prevAbbreviation = abbreviations[0]
            for tmpAbbreviation in abbreviations {
                if (startValue < tmpAbbreviation.threshold) {
                    break
                }
                prevAbbreviation = tmpAbbreviation
            }
            return prevAbbreviation
        } ()
        
        let value = Double(num.doubleValue) / abbreviation.divisor
        numFormatter.positiveSuffix = abbreviation.suffix
        numFormatter.negativeSuffix = abbreviation.suffix
        numFormatter.allowsFloats = true
        numFormatter.minimumIntegerDigits = 1
        numFormatter.minimumFractionDigits = 0
        numFormatter.maximumFractionDigits = 1
        return numFormatter.string(from: NSNumber.init(value: value))
            
            
        }else{
            return nil
        }
    }
}

//MARK: - current Device model -> Strig

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}







