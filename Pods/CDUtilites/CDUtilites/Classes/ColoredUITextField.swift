//
//  coloredUITextField.swift
//  Pokechat
//
//  Created by new user on 9/8/16.
//  Copyright © 2016 com.mihailsalari. All rights reserved.
//

import UIKit

let colorViolet : UIColor = .init(red: 60/255, green: 40/255, blue: 70/255, alpha: 1.0)
let colorRoz    : UIColor = .init(red: 249/255, green: 56/255, blue: 74/255, alpha: 1.0)
let colorGray   : UIColor = .init(red: 171/255, green: 171/255, blue: 171/255, alpha: 1.0)
let thisdevice = UIDevice.current.userInterfaceIdiom

public struct IconFieldStyle
{
    let selectedColor   : UIColor
    let normalColor     : UIColor
    let contentMode     : UIView.ContentMode
    let size            : CGSize
    let fieldViewMode   : UITextField.ViewMode
    
    public init(selectedColor   : UIColor = .red,
                normalColor     : UIColor = .blue,
                contentMode     : UIView.ContentMode = .center,
                size            : CGSize = CGSize(width: 34, height: 23),
                fieldViewMode   : UITextField.ViewMode = .always)
    {
        self.selectedColor = selectedColor
        self.normalColor = normalColor
        self.contentMode = contentMode
        self.size = size
        self.fieldViewMode = fieldViewMode
    }
}



public extension ColoredUITextField
{
    @IBInspectable var defaultTextColor : UIColor
        {
        get{ return defaultTxtColor }
        set{ defaultTxtColor = newValue }
    }
    
    @IBInspectable var selectedColorBorder : UIColor
        {
        get{ return selectedColor }
        set{ selectedColor = newValue }
    }
    
    @IBInspectable var defaulColorBorder : UIColor
        {
        get{ return defaultColor }
        set{ defaultColor = newValue }
    }

}

open class ColoredUITextField: UITextField
{
    public var padding = UIEdgeInsets(top: 0, left: 44, bottom: 0, right: 5);
    
    fileprivate var defaultTxtColor : UIColor = colorViolet
    fileprivate var selectedColor   : UIColor = colorRoz
    fileprivate var defaultColor    : UIColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
    
    public var isActive:Bool = false {
        didSet{
            self.tintColor = self.isActive ? self.selectedColorBorder : self.defaulColorBorder
            self.textColor = self.isActive ? self.selectedColorBorder : self.defaultTextColor
            self.borderColor = self.isActive ? self.selectedColorBorder : self.defaulColorBorder
//            self.setBottomBorder(color: self.isActive ? self.selectedColorBorder : self.defaulColorBorder)
            if let img :UIImageView = self.leftView as? UIImageView
            {
                img.isHighlighted = self.isActive
            }
        }
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.tintColor = defaulColorBorder
        self.font = UIFont(name: ".SFUIText-Regular", size: thisdevice == .pad ? 20 : 18)
        self.isActive = false
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.tintColor = defaulColorBorder
        self.font = UIFont(name: ".SFUIText-Regular", size: thisdevice == .pad ? 20.0 : 18.0)
        self.isActive = false
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.borderColor = self.isActive ? self.selectedColorBorder : self.defaulColorBorder
//        self.setBottomBorder(color: self.isActive ? self.selectedColorBorder : self.defaulColorBorder)
    }
    
    private func setBottomBorder(color:UIColor)
    {
        self.borderStyle = UITextField.BorderStyle.none;
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.self.bounds.height - width,   width:  self.bounds.size.width, height: self.bounds.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    private func setLeftBorder(color:UIColor)
    {
        self.borderStyle = UITextField.BorderStyle.none;
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0,   width:  width, height: self.bounds.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }

}

public extension ColoredUITextField
{
    func setLeftIconView(nameImg:String, iconStyle: IconFieldStyle = IconFieldStyle())
    {
        self.leftView = getIconField(nameImg: nameImg, iconStyle: iconStyle)
        self.leftViewMode = iconStyle.fieldViewMode
    }
    
    func getIconField(nameImg:String, iconStyle: IconFieldStyle = IconFieldStyle()) -> UIImageView
    {
        let arrow = UIImageView(image: UIImage(named: nameImg)?.bma_tintWithColor(color: iconStyle.normalColor))
        arrow.highlightedImage = UIImage(named: nameImg)?.bma_tintWithColor(color: iconStyle.selectedColor)
        arrow.frame = CGRect.init(origin: CGPoint.zero, size: iconStyle.size)
        arrow.contentMode = iconStyle.contentMode
        return arrow
    }
}

