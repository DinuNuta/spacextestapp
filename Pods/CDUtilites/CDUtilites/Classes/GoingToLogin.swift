//
//  GoingToLogin.swift
//  Evrica
//
//  Created by Coscodan Dinu on 6/20/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation

public protocol GoingToLoginProtocol
{
    func showAlertTokenError(title:String, message:String)
}

//extension GoingToLoginProtocol where Self:UIViewController
//{
//    func showAlertTokenError(title:String, message:String)
//    {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(action: UIAlertAction!) in
//
//            self.sideMenuController?.toLoginScreen()
//        
//        }))
//        DispatchQueue.main.async {
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
//}
