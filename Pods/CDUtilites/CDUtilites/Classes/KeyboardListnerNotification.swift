//
//  KeyboardListnerNotification.swift
//  Evrica
//
//  Created by Coscodan Dinu on 6/21/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation


public protocol KeyboardApearDissappearProtocol : class
{
    func subscribeOnKeyboardNotification()
    func removeObservers()
    func keyboardAnimationWith(height:CGFloat, duration:TimeInterval, curve:UIView.AnimationCurve, reverse:Bool)
    var ishidenVC : Bool {get set}
    var obserHide : NSObjectProtocol? {get set}
    var observerShow : NSObjectProtocol? {get set}
    
    var butomConstraintsForKeyboard: NSLayoutConstraint? {get set}
}


public extension KeyboardApearDissappearProtocol where Self: UIViewController
{
    
    
    func subscribeOnKeyboardNotification()
    {
        self.observerShow = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: OperationQueue.main, using: keyBoardWillHide(notification:))
        self.obserHide = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: OperationQueue.main, using: keyBoardWillAppear(notification:))
    }
    
    func removeObservers()
    {
        NotificationCenter.default.removeObserver(self.observerShow ?? self)
        NotificationCenter.default.removeObserver(self.obserHide ?? self)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self)
        self.obserHide = nil
        self.observerShow = nil
    }
    
    func keyBoardWillAppear(notification:Notification)
    {
        if !ishidenVC {
            keyboardAnimateWith(notification: notification, reverse: false)
        }
    }
    
    func keyBoardWillHide(notification:Notification)
    {
        if !ishidenVC {
            keyboardAnimateWith(notification: notification, reverse: true)
        }
    }
    
    func keyboardAnimateWith(notification:Notification,reverse:Bool)
    {
        guard let userInfo = notification.userInfo,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let rawAnimationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
            let animationCurve  =  UIView.AnimationCurve.init(rawValue: Int(truncating: rawAnimationCurve))
            else
        {
            print(notification.userInfo?.description ?? "")
            return
        }
//        let frame = view.convert(keyboardEndFrame, from: view.window)
//        let height = view.frame.maxY - frame.minY        
        let height = keyboardFrame.height
        keyboardAnimationWith(height:height, duration: duration, curve: animationCurve, reverse: reverse)
    }
    
    func keyboardAnimationWith(height:CGFloat, duration:TimeInterval, curve:UIView.AnimationCurve, reverse:Bool)
    {
        let tab = self.tabBarController?.tabBar.frame.height ?? 0
        butomConstraintsForKeyboard?.constant = reverse ? 0 : (height - tab)
        UIView.animate(withDuration: duration, animations: {[weak self] in self?.view.layoutIfNeeded()})
    }

}
