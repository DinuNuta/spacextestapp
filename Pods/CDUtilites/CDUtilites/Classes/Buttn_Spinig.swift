//
//  Buttn_Spinig.swift
//  CDUtilites
//
//  Created by Coscodan Dinu on 9/13/18.
//

import Foundation


open class Button_Spining: UIButton {
    
    open var activityIndicator: UIActivityIndicatorView!
    open func showLoading()
    {
        DispatchQueue.main.async
            {
                self.setTitle("", for: .normal)
                if self.activityIndicator == nil
                {
                    self.activityIndicator = self.createActivityIndicator()
                }
                self.showSpining()
        }
        
    }
    
    open func hideLoading()
    {
        if let activity = activityIndicator
        {
            activity.stopAnimating()
            activity.removeFromSuperview()
        }
    }
    
    private func showSpining()
    {
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.activityIndicator)
        self.bringSubviewToFront(self.activityIndicator)
        self.centerActivityInButton()
        self.activityIndicator.startAnimating()
        
    }
    
    private func centerActivityInButton()
    {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX,
                                                   relatedBy: .equal, toItem: activityIndicator,
                                                   attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY,
                                                   relatedBy: .equal, toItem: activityIndicator,
                                                   attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView
    {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .blue
        return activityIndicator
    }
}
