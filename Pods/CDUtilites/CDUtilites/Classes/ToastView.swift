//
//  InfoUserView.swift
//  Evrica
//
//  Created by Coscodan Dinu on 6/6/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation


public struct StyleToast
{
    let backgroundColor : UIColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    let textColor : UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    let height : CGFloat = 54
    public init(){}    
}

public class ToastView: UIView, CustomView
{
    public var view    : UIView!
    var style   : StyleToast = StyleToast()
    {
        didSet{
            self.backgroundColor = style.backgroundColor
            self.textLabel.textColor = style.textColor
            self.frame.size.height = style.height
        }
    }
    var text    : String = ""
    {
        didSet{
            textLabel.text = text
        }
    }
    
    @IBOutlet weak var textLabel: UILabel!
    
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        xibSetup()
    }
    init() {
        super.init(frame: CGRect.zero)
    }
 
}
