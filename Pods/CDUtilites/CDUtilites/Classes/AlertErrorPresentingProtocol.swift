//
//  AlertPresenterProtocol.swift
//  Evrica
//
//  Created by Coscodan Dinu on 6/8/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation

public protocol AlertErrorPresenting: ActivitYPresentingProtocol
{
    func showAlertError(title:String, message:String)
    func showAlertError(error:Error)
    func showAlertError(alert:UIAlertController, animated: Bool, completion: (()->())?)
    func didShowAlertError()
    var mainViewController: UIViewController? {get}
}

//extension AlertErrorPresenting {
//    func showAlertError(error:Error)
//    {
//        var title = "Error"
//        var message = error.localizedDescription
//        switch error {
//        case DVError.networkError(code: _,description: let messages):
//            message = messages
//            title = "Network Error"
//        case DVError.internalError(error: _, description: let messages):
//            message = messages
//            title = "Internal Error"
//        default:
//            message = error.localizedDescription
//        }
//
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(action: UIAlertAction!) in}))
//
//        logger.error(message)
//
//        DispatchQueue.main.async {
//            self.showAlertError(alert: alert, animated: true, completion: nil)
//        }
//        didShowAlertError()
//    }
//
//    func showAlertError(title:String, message:String)
//    {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(action: UIAlertAction!) in}))
//        logger.error(message)
//        DispatchQueue.main.async {
//            self.showAlertError(alert: alert, animated: true, completion: nil)
//        }
//        didShowAlertError()
//    }
//
//    func didShowAlertError()
//    {
//        print("didShowAlertError")
//    }
//}

public extension AlertErrorPresenting where Self:UIViewController
{
    
    func showAlertError(alert:UIAlertController, animated: Bool=true, completion: (()->())? = nil)
    {
        DispatchQueue.main.async {
            self.present(alert, animated: animated, completion: completion)
        }
        didShowAlertError()
    }
    
    var mainViewController: UIViewController? {
        get{
            return self
        }
    }
}

public extension AlertErrorPresenting
{
    func showAlertError(alert:UIAlertController, animated: Bool=true, completion: (()->())? = nil)
    {
        self.hideActivity()
        if let mainVC = UIApplication.topViewController(){
            DispatchQueue.main.async {
                mainVC.present(alert, animated: true, completion: nil)
            }
            didShowAlertError()
        }
    }
    
    var mainViewController: UIViewController? {
        get{
            return UIApplication.topViewController()
        }
    }
}

