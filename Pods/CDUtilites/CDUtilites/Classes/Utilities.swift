//
//  Utilities.swift
//  Evrica
//
//  Created by Coscodan Dinu on 5/23/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation


public extension Notification.Name
{
    static let toLogin = Notification.Name("toLoginScreen")
}


/// The 'DateManager' struct represents utilites for get Date or String with string or date and Formater
///
public struct DateManager
{
    /// get date from string
    ///
    /// - Parameter string: String of date.
    /// - Returns: Date or nil if no create date with string
    static func date(from string:String) -> Date?
    {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = formater.date(from: string)
        return date
    }
    
    /// get string from date
    ///
    /// - Parameter date: Date
    /// - Returns: return string represntation of date with formater `yyyy-MM-dd'T'HH:mm:ss.SSSZ`
    static func string(from date:Date) -> String?
    {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formater.string(from: date)
    }
}

// MARK: - assign Bool with condition
public extension Bool
{
    mutating func assingn(if bool:Bool) {
        self.assingn(if: bool, isA: true)
    }
    
    mutating func assingn(ifNot bool:Bool) {
        self.assingn(if: bool, isA: false)
    }
    
    mutating func assingn(if bool:Bool, isA:Bool) {
        if bool == isA { self = bool }
    }
}

public extension String {

    var length: Int {
        return self.count
    }

    subscript (i: Int) -> String {
        return self[(i ..< i + 1)]
    }

    func substring(from: Int) -> String {
        return self[min(from, length) ..< length]
    }

    func substring(to: Int) -> String {
        return self[0 ..< max(0, to)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }

}

public func groupby<T, H: Hashable>(items: [T], f: (T) -> H) -> [H: [T]] {
    
    return items.reduce([:]) { ( ac:[H: [T]],o:T) -> [H: [T]] in
        var ac = ac
        let h = f(o)
        if var c = ac[h] {
            c.append(o)
            ac.updateValue(c, forKey: h)
        } else {
            ac.updateValue([o], forKey: h)
        }
        return ac
    }
}


public extension String {
    static let numberFormatter = NumberFormatter()
    var doubleValue: Double {
        String.numberFormatter.decimalSeparator = "."
        if let result =  String.numberFormatter.number(from: self) {
            return result.doubleValue
        } else {
            String.numberFormatter.decimalSeparator = ","
            if let result = String.numberFormatter.number(from: self) {
                return result.doubleValue
            }
        }
        return 0
    }
}
public extension NumberFormatter {
    convenience init(style: Style) {
        self.init()
        self.numberStyle = style
    }
}
public extension Formatter {
    static let currency = NumberFormatter(style: .currency)
}
public extension FloatingPoint {
    var currency: String {
        return Formatter.currency.string(for: self) ?? ""
    }
}

public extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
// button Done fore hide keyboard
//extension UITextField
//{
//    open override func awakeFromNib() {
//        super.awakeFromNib()
//        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.frame.size.width, height: 30))
//        //create left side empty space so that done button set on right side
//        toolbar.backgroundColor = .clear
//        toolbar.isTranslucent = true
//        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
//        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(UITextField.doneButtonAction))
//        toolbar.setItems([flexSpace, doneBtn], animated: false)
//        toolbar.sizeToFit()
//
//        self.inputAccessoryView = toolbar
//    }
//    @objc func doneButtonAction() {
//        self.resignFirstResponder()
//    }
//}


//MARK: - run block after delay
///
/// Run block after delay
///
/// - Parameters:
///   - delay: Double: delay seconds
///   - closure: `()->()` Block that will take place over delay time
public func delay(delay:Double, closure:@escaping ()->())
{
    DispatchQueue.main.asyncAfter(deadline: .now() + delay)
    {
        closure()
    }
}
