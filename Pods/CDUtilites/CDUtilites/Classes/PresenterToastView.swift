//
//  PresenterToastView.swift
//  Evrica
//
//  Created by Coscodan Dinu on 6/7/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation

public protocol ToastPresenting: class{}

public extension ToastPresenting where Self: UIViewController
{
    func showToast(text: String, style: StyleToast = StyleToast())
    {
        let viewFrame = CGRect.init(x: 0, y: -style.height, width: view.frame.size.width, height: style.height)
        let viewToast : ToastView = ToastView.init(frame: viewFrame)
        viewToast.style = style
        viewToast.text = text
        viewToast.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewToast)
        view.bringSubviewToFront(viewToast)
        let verticalConstraint = NSLayoutConstraint(item: viewToast, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: -style.height)
        let horizontalConstraintLeft = NSLayoutConstraint(item: viewToast, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0)
        let horizontalConstraintRight = NSLayoutConstraint(item: viewToast, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: viewToast, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44)
        
        view.addConstraints([verticalConstraint,heightConstraint,horizontalConstraintLeft,horizontalConstraintRight])
        view.layoutIfNeeded()
        
        var barHeight = 0.0
        if let _ = self.navigationController?.navigationBar.isTranslucent
        {
            barHeight = 44.0+22.0
        }
        
        UIView.animate(withDuration: 0.6, animations: {
            viewToast.isHidden = false
            verticalConstraint.constant = CGFloat(barHeight)
            self.view.layoutIfNeeded()
        }, completion: {finished in  delay(delay: 1){
            UIView.animate(withDuration:0.4, animations: {
                verticalConstraint.constant = -style.height
                self.view.layoutIfNeeded()
            }, completion: {finished in
                viewToast.removeFromSuperview()
            })
            }
        })
    }
}

