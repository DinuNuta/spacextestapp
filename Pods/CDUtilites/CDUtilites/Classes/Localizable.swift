//
//  Localizable.swift
//  CDUtilites
//
//  Created by Coscodan Dinu on 11/13/18.
//

import Foundation

public protocol Localizable {
    var localized: String { get }
}
public extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

public protocol XIBLocalizable {
    var xibLocKey: String? { get set }
}

extension UILabel: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
    }
}

extension UITextField: XIBLocalizable{
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
    }
}
