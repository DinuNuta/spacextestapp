# CDUtilites

[![CI Status](https://img.shields.io/travis/DinuNuta/CDUtilites.svg?style=flat)](https://travis-ci.org/DinuNuta/CDUtilites)
[![Version](https://img.shields.io/cocoapods/v/CDUtilites.svg?style=flat)](https://cocoapods.org/pods/CDUtilites)
[![License](https://img.shields.io/cocoapods/l/CDUtilites.svg?style=flat)](https://cocoapods.org/pods/CDUtilites)
[![Platform](https://img.shields.io/cocoapods/p/CDUtilites.svg?style=flat)](https://cocoapods.org/pods/CDUtilites)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CDUtilites is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CDUtilites'
```

## Author

DinuNuta, coscodan.dinu@gmail.com

## License

CDUtilites is available under the MIT license. See the LICENSE file for more info.
