//
//  FakeRequestSender.swift
//  SpaceXTests
//
//  Created by Coscodan Dinu on 11/23/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDNetwork
import CDResult

enum FakeRequestSenderError: Error {
    case connectionError
}

class FakeRequestSender: CDRequestSending {
    var errorInterpreter: CDErrorInterpreting?
    
    var stubData: Data?
    var url: URLComponents?
    var requestMethod: HttpMethod?
    var isCalled_sendReq: Bool = false
    var error: FakeRequestSenderError?
    
    func send(request: CDRequestType, completion: ((Result<Data?>) -> ())?) {
        isCalled_sendReq = true
        url = request.request.url.flatMap{URLComponents(url: $0, resolvingAgainstBaseURL: true)}
        requestMethod = request.method
        
        if let error = error {
            completion?(.error(error))
        } else {
            completion?(.success(stubData))
        }
    }
}
