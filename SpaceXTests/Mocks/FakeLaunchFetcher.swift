//
//  FakeLaunchFetcher.swift
//  SpaceXTests
//
//  Created by Coscodan Dinu on 11/23/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import CDResult
@testable import SpaceX

class FakeLaunchFetcher: LaunchFetching {
    
    var stubData: Data?
    
    func fetchLaunches(with completion: @escaping (Result<[Launch]>) -> ()) {
        do {
            let launches = try [Launch](data: stubData)
            completion(.success(launches))
        } catch  {
            completion(.error(error))
        }
    }
}
