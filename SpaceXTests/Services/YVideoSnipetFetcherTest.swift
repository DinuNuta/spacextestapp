//
//  YVideoSnipetFetcherTest.swift
//  SpaceXTests
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import XCTest
@testable import SpaceX

class YVideoSnipetFetcherTest: XCTestCase {
    var requestSender = FakeRequestSender()
    var sut: YVideoSnipetFetcher!
    
    override func setUp() {
        super.setUp()
        sut = YVideoSnipetFetcher(requestSender: requestSender)
        
    }
    
    override func tearDown() {
        super.tearDown()
        requestSender.error = nil
        requestSender.stubData = nil
    }
    
    func testItShouldCallRequestManager() {
        sut.fetchVideos(with: []){ _ in }
        XCTAssert(requestSender.isCalled_sendReq)
    }
    
    func testItShoudProvideCorrectUrlAndHTTPMethod(){
        correctUrlAndHTTPMethod()
    }
    
    func correctUrlAndHTTPMethod(with ids:[String]=["RUjH14vhLxA","Lk4zQ2wP-Nc"]) {
        sut.fetchVideos(with:ids){_ in}
        XCTAssert(requestSender.requestMethod! == .GET)
        XCTAssertNotNil(requestSender.url)
        var url = requestSender.url
        url?.queryItems = nil
        XCTAssert(url!.url! == URLs.yVideos)
        
        let urlComponents = requestSender.url
        XCTAssertNotNil(urlComponents?.queryItems?.first(where:{ $0.name == YAC.reqParam.key.rawValue})?.value)
        let part = urlComponents?.queryItems?.first(where: {$0.name==YAC.reqParam.part.rawValue})?.value
        XCTAssertEqual(part, YAC.reqParam.part._defValue)
        let strids = urlComponents?.queryItems?.first(where: {$0.name==YAC.reqParam.id.rawValue})?.value
        XCTAssertEqual(ids.joined(separator: ","), strids)
    }

    
    func testItReturnedVideoResponseWithValidData() {
        let expect = expectation(description: "It should return valid VideoItem response")
        let testBundle = Bundle(for:FakeRequestSender.self)
        let path = testBundle.path(forResource: "FakeYVideoSnipetsResponse", ofType: "json")!
        
        requestSender.stubData = try? Data(contentsOf: URL(fileURLWithPath: path))
        
        sut.fetchVideos(with: []){ (result) in
            do {
                let response: YVideoListResponse = try result.resolve()
                XCTAssertFalse(response.items.isEmpty)
            } catch {
                XCTFail(expect.description + " " + error.localizedDescription)
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            if error != nil {
                XCTFail( expect.description + " " + error!.localizedDescription)
            }
        }
    }
    
}
