//
//  LaunchFetcherTests.swift
//  SpaceXTests
//
//  Created by Coscodan Dinu on 11/23/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import XCTest
@testable import SpaceX

class LaunchFetcherTests: XCTestCase {

    var requestSender = FakeRequestSender()
    var sut: LaunchFetcher!
    
    override func setUp() {
        super.setUp()
        sut = LaunchFetcher(requestSender: requestSender)
        
    }

    override func tearDown() {
        super.tearDown()
        requestSender.error = nil
        requestSender.stubData = nil
    }

    
    func testItShouldCallRequestManager() {
        sut.fetchLaunches{ _ in }
        XCTAssert(requestSender.isCalled_sendReq)
    }
    
    func testItShoudProvideCorrectUrlAndHTTPMethod() {
        sut.fetchLaunches{_ in}
        XCTAssert(requestSender.requestMethod! == .GET)
        XCTAssertNotNil(requestSender.url)
        var url = requestSender.url
        url?.queryItems = nil
        XCTAssert(url!.url! == URLs.launches)
        
//        let ordered = URLQueryItem(name: "order", value: "desc")
//        XCTAssert(requestSender.url!.queryItems!.contains(ordered))
    }
    
    
    func testItReturnedValidLaunchessArrayWithValidData() {
        let expect = expectation(description: "It should return valid array of launches")
        let testBundle = Bundle(for:FakeRequestSender.self)
        let path = testBundle.path(forResource: "FakeLaunchDataJSON", ofType: "json")!
        
        requestSender.stubData = try? Data(contentsOf: URL(fileURLWithPath: path))
        
        sut.fetchLaunches { (result) in
            do {
                let launches: [Launch] = try result.resolve()
                XCTAssertFalse(launches.isEmpty)
            } catch {
                XCTFail(expect.description + " " + error.localizedDescription)
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            if error != nil {
                XCTFail( expect.description + " " + error!.localizedDescription)
            }
        }
    }
    
    func testItShouldReturnAnErrorIfDataIsNil() {
        let expect = expectation(description: "It should return an error if data is nil")
        requestSender.stubData = nil
        sut.fetchLaunches(with: { (result) in
            do {
                XCTAssertThrowsError(try result.resolve(), "should throw an error", { error in
                    print(error.localizedDescription)
                })
            } catch {
                print(error)
            }
            expect.fulfill()
        })
        waitForExpectations(timeout: 1) { (error) in
            if error != nil {
                XCTFail( expect.description + " " + error!.localizedDescription)
            }
        }
    }
    
    func testItShouldReturnErrorIfDataIsNotValid() {
        let expect = expectation(description: "It should return an error if data is not valid")
        requestSender.stubData = "{ \"wrongData\" : 1 }".data(using: .utf8)
        sut.fetchLaunches { result in
            do {
                XCTAssertThrowsError(try result.resolve(), "should throw an error", { error in
                    print(error.localizedDescription)
                })
            } catch {}
            expect.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            if error != nil {
                XCTFail( expect.description + " " + error!.localizedDescription)
            }
        }
    }
    
    
    func testItShouldReturnAnEmptyArrayIfDataIsEmpty() {
        let expect = expectation(description: "It should return an empty array if data is empty")
        requestSender.stubData = "[]".data(using: .utf8)
        sut.fetchLaunches { result in
            do {
                let launches = try result.resolve()
                XCTAssertTrue(launches.isEmpty)
            } catch {
                XCTFail(expect.description + " " + error.localizedDescription)
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            if error != nil {
                XCTFail( expect.description + " " + error!.localizedDescription)
            }
        }
    }
    
    func testItShouldPropagateServerErrors() {
        let expect = expectation(description: "It should propagate server errors")
        requestSender.stubData = nil
        requestSender.error = FakeRequestSenderError.connectionError
        sut.fetchLaunches { result in
            do {
                let _ = try result.resolve()
                XCTFail("should throw an error")
            } catch {
                XCTAssertNotNil((error as? FakeRequestSenderError))
                XCTAssert((error as! FakeRequestSenderError) == .connectionError)
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            if error != nil {
                XCTFail( expect.description + " " + error!.localizedDescription)
            }
        }
    }
    
    
}
