//
//  LaunchTest.swift
//  SpaceXTests
//
//  Created by Coscodan Dinu on 11/22/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import XCTest
@testable import SpaceX

class LaunchTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testShouldCreatedCorrectFieldsFromJSON() {
        let testBundle = Bundle(for:FakeRequestSender.self)
        let path = testBundle.path(forResource: "FakeSingleLaunchDataJson", ofType: "json")!
        let data = try! Data.init(contentsOf: URL.init(fileURLWithPath: path))
        
        do {
            let launch = try Launch(data: data)
            
            XCTAssertEqual(launch.flightNumber, 17)
            XCTAssertEqual(launch.missionName, "AsiaSat 6")
            XCTAssertEqual(launch.missionIDs[0], "593B499")
            XCTAssertEqual(launch.lunchYear, "2014")
            XCTAssertEqual(launch.launchDateUnix, 1410066000)
            XCTAssertEqual(launch.launchDateUtc, "2014-09-07T05:00:00.000Z")
            XCTAssertEqual(launch.launchDateLocal, "2014-09-07T01:00:00-04:00")
            XCTAssertFalse(launch.isTentative)
            XCTAssertEqual(launch.tentativeMaxPrecision, "hour")
            XCTAssertFalse(launch.tbd)
            XCTAssertTrue(launch.launchSuccess)
            
            XCTAssertEqual(launch.links.missionPatch, "https://images2.imgbox.com/57/6a/upI6gwfq_o.png")
            XCTAssertEqual(launch.links.missionPatchSmall, "https://images2.imgbox.com/6f/c0/D3Owbmpo_o.png")
            XCTAssertEqual(launch.links.redditLaunch, "http://www.reddit.com/r/spacex/comments/2fenrv")
            XCTAssertEqual(launch.links.presskit, "https://www.spaceflightnow.com/falcon9/012/presskit.pdf")
            XCTAssertEqual(launch.links.articleLink, "https://www.space.com/27052-spacex-launches-asiasat6-satellite.html")
            XCTAssertEqual(launch.links.wikipedia, "https://en.wikipedia.org/wiki/AsiaSat_6")
            XCTAssertEqual(launch.links.videoLink, "https://www.youtube.com/watch?v=39ninsyTRk8")
            
            XCTAssertNil(launch.links.redditCampaign)
            XCTAssertNil(launch.links.redditMedia)
            XCTAssertNil(launch.links.redditRecovery)
            
            XCTAssertEqual(launch.links.flickrImages[0], "https://farm8.staticflickr.com/7604/16169087563_0e3559ab5b_o.jpg")
            XCTAssertEqual(launch.links.flickrImages[1], "https://farm9.staticflickr.com/8742/16233828644_96738200b2_o.jpg")
            XCTAssertEqual(launch.links.flickrImages[2], "https://farm8.staticflickr.com/7645/16601443698_e70315d1ed_o.jpg")
            XCTAssertEqual(launch.links.flickrImages[3], "https://farm9.staticflickr.com/8730/16830335046_5f017c17be_o.jpg")
            XCTAssertEqual(launch.links.flickrImages[4], "https://farm9.staticflickr.com/8637/16855040322_57671ab8eb_o.jpg")
            
            XCTAssertNil(launch.details)
            XCTAssertFalse(launch.upcoming)
            XCTAssertEqual(launch.staticFireDateUtc, "2014-08-22T23:51:18.000Z")
            XCTAssertEqual(launch.staticFireDateUnix, 1408751478)
            
            XCTAssertEqual(launch.rocket.id, "falcon9")
            XCTAssertEqual(launch.rocket.name, "Falcon 9")
            XCTAssertEqual(launch.rocket.rocketType, "v1.1")
            XCTAssertEqual(launch.rocket.firstStage.cores[0].coreSerial, "B1011")
            XCTAssertEqual(launch.rocket.firstStage.cores[0].flight, 1)
            XCTAssertEqual(launch.rocket.firstStage.cores[0].block, 1)
            XCTAssertFalse(launch.rocket.firstStage.cores[0].gridfins)
            XCTAssertFalse(launch.rocket.firstStage.cores[0].legs)
            XCTAssertFalse(launch.rocket.firstStage.cores[0].reused)
            XCTAssertNil(launch.rocket.firstStage.cores[0].landSuccess)
            XCTAssertEqual(launch.rocket.firstStage.cores[0].landingIntent, false)
            XCTAssertNil(launch.rocket.firstStage.cores[0].landingType)
            XCTAssertNil(launch.rocket.firstStage.cores[0].landingVehicle)
            XCTAssertEqual(launch.rocket.secondStage.block, 1)
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].payloadID, "AsiaSat 6")
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].noradID[0], 40141)
            XCTAssertFalse(launch.rocket.secondStage.payloads[0].reused)
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].customers[0], "AsiaSat")
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].nationality, "Hong Kong")
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].manufacturer, "SSL")
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].payloadType, "Satellite")
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].payloadMassKg, 4428)
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].payloadMassLbs, 9762)
            XCTAssertEqual(launch.rocket.secondStage.payloads[0].orbit, "GTO")
            XCTAssertNotNil(launch.rocket.fairings)
            XCTAssertFalse(launch.rocket.fairings!.reused)
            XCTAssertFalse(launch.rocket.fairings!.recoveryAttempt)
            XCTAssertFalse(launch.rocket.fairings!.recovered)
            XCTAssertNil(launch.rocket.fairings!.ship)
            XCTAssertTrue(launch.ships.isEmpty)
            XCTAssertNil(launch.telemetry.flightClub)
            XCTAssertFalse(launch.reuse.core)
            XCTAssertFalse(launch.reuse.sideCore1)
            XCTAssertFalse(launch.reuse.sideCore2)
            XCTAssertFalse(launch.reuse.fairings)
            XCTAssertFalse(launch.reuse.capsule)
            XCTAssertEqual(launch.launchSite.siteID, "ccafs_slc_40")
            XCTAssertEqual(launch.launchSite.siteName, "CCAFS SLC 40")
            XCTAssertEqual(launch.launchSite.siteNameLong, "Cape Canaveral Air Force Station Space Launch Complex 40")
            
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func testItShouldThrowAnErrorIfDataIsNil() {
        XCTAssertThrowsError(try Launch(data: nil), "shold throw error with description that data is nil") { (error) in
            print(error)
        }
    }
    
    func testItShouldThrowAnErrorIfDataIsInvalid() {
        let data = "{\"invalidData\" : 0}".data(using: .utf8)
        XCTAssertThrowsError(try Launch(data: data), "shold throw a decoding error") { (error) in
            print(error)
        }
    }
  
}
