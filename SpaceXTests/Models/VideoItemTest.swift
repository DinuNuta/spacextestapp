//
//  VideoItemTest.swift
//  SpaceXTests
//
//  Created by Coscodan Dinu on 11/24/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import XCTest
@testable import SpaceX

class VideoItemTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testShouldCreatedCorrectFieldsFromJSON() {
        let testBundle = Bundle(for:FakeRequestSender.self)
        let path = testBundle.path(forResource: "FakeYVideoSnipetData", ofType: "json")!
        let data = try! Data.init(contentsOf: URL.init(fileURLWithPath: path))
        
        do {
            let videoItem = try VideoItem(data: data)
            
            XCTAssertEqual(videoItem.kind, "youtube#video")
            XCTAssertEqual(videoItem.etag, "\"XI7nbFXulYBIpL0ayR_gDh3eu1k/1BIMrywRmrbeQpSjHeDBxPLedIE\"")
            XCTAssertEqual(videoItem.id, "RUjH14vhLxA")
            
            XCTAssertEqual(videoItem.snippet.publishedAt, "2017-10-11T23:32:47.000Z")
            XCTAssertEqual(videoItem.snippet.channelID, "UCtI0Hodo5o5dUb67FeUjDeA")
            XCTAssertEqual(videoItem.snippet.title, "Koreasat-5A Webcast")
            XCTAssertEqual(videoItem.snippet.description, "SpaceX is targeting launch of Koreasat-5A from Launch Complex 39A (LC-39A) at NASA’s Kennedy Space Center")
            
            XCTAssertEqual(videoItem.snippet.thumbnails._default!.url, "https://i.ytimg.com/vi/RUjH14vhLxA/default.jpg")
            XCTAssertEqual(videoItem.snippet.thumbnails._default!.width, 120)
            XCTAssertEqual(videoItem.snippet.thumbnails._default!.height, 90)
            XCTAssertEqual(videoItem.snippet.thumbnails.medium!.url, "https://i.ytimg.com/vi/RUjH14vhLxA/mqdefault.jpg")
            XCTAssertEqual(videoItem.snippet.thumbnails.medium!.width, 320)
            XCTAssertEqual(videoItem.snippet.thumbnails.medium!.height, 180)
            XCTAssertEqual(videoItem.snippet.thumbnails.high!.url, "https://i.ytimg.com/vi/RUjH14vhLxA/hqdefault.jpg")
            XCTAssertEqual(videoItem.snippet.thumbnails.high!.width, 480)
            XCTAssertEqual(videoItem.snippet.thumbnails.high!.height, 360)
            XCTAssertEqual(videoItem.snippet.thumbnails.standard!.url, "https://i.ytimg.com/vi/0a_00nJ_Y88/sddefault.jpg")
            XCTAssertEqual(videoItem.snippet.thumbnails.standard!.width, 640)
            XCTAssertEqual(videoItem.snippet.thumbnails.standard!.height, 480)
            XCTAssertEqual(videoItem.snippet.thumbnails.maxres!.url, "https://i.ytimg.com/vi/yTaIDooc8Og/maxresdefault.jpg")
            XCTAssertEqual(videoItem.snippet.thumbnails.maxres!.width, 1280)
            XCTAssertEqual(videoItem.snippet.thumbnails.maxres!.height, 720)
            
            XCTAssertEqual(videoItem.snippet.channelTitle, "SpaceX")
            XCTAssertEqual(videoItem.snippet.categoryID, "28")
            XCTAssertEqual(videoItem.snippet.liveBroadcastContent, "none")
            XCTAssertEqual(videoItem.snippet.localized!.title, "Koreasat-5A Webcast")
            XCTAssertEqual(videoItem.snippet.localized!.description, "SpaceX is targeting launch of Koreasat-5A from Launch Complex 39A (LC-39A) at NASA’s Kennedy Space Center")
            
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testItShouldThrowAnErrorIfDataIsNil() {
        XCTAssertThrowsError(try VideoItem(data: nil), "shold throw error with description that data is nil") { (error) in
            print(error)
        }
    }
    
    func testItShouldThrowAnErrorIfDataIsInvalid() {
        let data = "{\"invalidData\" : 0}".data(using: .utf8)
        XCTAssertThrowsError(try VideoItem(data: data), "shold throw a decoding error") { (error) in
            print(error)
        }
    }

}
